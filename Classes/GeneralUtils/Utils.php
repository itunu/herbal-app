<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 9:20 AM
 */

namespace Utils;

class Utils
{
    public $admin;
    public static function validateInput(array $input, array $compulsoryFields) {
        if(empty($input)){
            return ['Input Fields cannot be empty'];
        }
        $keys = array_keys($input);
        foreach($compulsoryFields as $field){
            if(!in_array($field,$keys) || strlen(trim($input[$field])) == 0){
                return [$field.' cannot be empty'];
            }
        }
        return [];
    }
//this function performs two functions in 1,
// 1. if there is a value to save into session, make the value in a storable value and then save it into session
//2. if there is no value , then convert back any value saved in that key to a php value, just like unstoring it
//3. u can use it to get the value of a stored session, or save a new session
    public  static  function setSession($key,$value= null)
    {
        if($key != null && $value != null){
            $_SESSION[$key] = serialize($value);
            return $value;
        }
        elseif ($value == null  && $key){
            return unserialize($_SESSION[$key]);
        }
        else{
            return false;

        }
    }

    public static function setFlashMessage($message) {
        self::setSession('message', $message);
    }

    public static function getFlashMessage() {
        $msg = self::setSession('message');
        self::unsetSessions('message');
        return $msg;
    }

    public  static  function isLoggedIn()
    {
        return self::setSession('loggedin');
    }

    public static  function  registerSession()
    {
        return self::setSession('loggedin',true);
    }

    public static function unsetSessions($key = null) {
        if($key) {
            unset($_SESSION[$key]);
        }else{
            session_destroy();
        }
    }

    public static  function setErrorMessage($code,$message){
        $displayMessage = [
            'status'=>'error',
            'data' =>[
                'code'=> $code,
                'message' => $message
            ]

        ];
        return $displayMessage;
    }

    public  static  function setSuccessMessage($message,$response){
        $displayMessage = [
            'status'=>'success',
            'data'=>[
                'message'=> $message,
                'response'=>$response
            ]
        ]  ;
        return $displayMessage;
    }

    public  function checkPermission($permission_code) {
        if (Utils::isLoggedIn('admin')) {
            $this->admin = Utils::setSession('admin');
        }
        $admin_permissions = $this->admin['permissions'];
        $keys = array_keys($admin_permissions);
        if(in_array($permission_code,$keys)){
            return true;
        }
        return false;
    }


}

