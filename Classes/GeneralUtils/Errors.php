<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 5:35 PM
 */

class Errors
{
    public $error_codes = [
        'E101'=> 'Admin already exist'
    ];

    public function getErrorMessage($code) {
            return $this->error_codes[$code];
    }

}