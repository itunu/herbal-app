<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 9:55 AM
 */
namespace Middlewares;
use \Interop\Container\ContainerInterface;

abstract class BaseMW
{
    protected  $container;

    public function __construct(ContainerInterface $c)
    {
        $this->container = $c;
    }
}