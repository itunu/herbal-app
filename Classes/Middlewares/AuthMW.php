<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 9:54 AM
 */

namespace Middlewares;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Utils\Utils;

class AuthMW extends BaseMW
{
    public function __invoke(Request $request,Response $response,$next)
    {
        // TODO: Implement __invoke() method.
        if(!Utils::isLoggedIn()) {
            $this->container->admin = Utils::setSession('admin');
            return $response->withStatus(302)->withHeader('location','/login');
        }
        $response = $next($request,$response);
        return $response;
    }
}