<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 8:01 PM
 */
namespace Models;
use Illuminate\Database\Eloquent\Model as Model;

class Recipe extends Model
{
    protected $fillable = ['label','description','herb_part_id', 'disease_id', 'status_id'];
    protected $guarded = ['id'];

    public  static function createRecipe($data) {
        $check = Recipe::where(['herb_part_id' =>$data['herb_part_id'], 'disease_id'=> $data['disease_id']])->first();
        if($check) {
            return $check;
        } else {
            $recipe = new Recipe($data);
            return ($recipe->save()) ? $recipe : null;
        }
    }

    public  static function  getRecipe($disease_id,$herb_part_id) {
        return Recipe::where(['disease_id'=> $disease_id, 'herb_part_id'=>$herb_part_id])->get();
    }
}