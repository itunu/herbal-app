<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 5:17 PM
 */
namespace Models;
use Illuminate\Database\Eloquent\Model as Emodel;

class Role extends Emodel
{
    protected $fillable =['label', 'description', 'status_id'];
    protected $guarded =['id'];

   public function Admin() {
       return $this->belongsToMany('Models\Admin');
   }

   public function Permission() {
       return $this->belongsToMany('Models\Permissions');
   }

   public static function createRole($data) {
        $new_role = new Role($data);
        $new_role->save();
        return $new_role;
   }

   public function editRole($updates) {
       $role = Role::find($updates['id']);
       $fields_to_update = ['label', 'description', 'status_id'];
       if(isset($role)) {
           foreach($fields_to_update as $fields) {
               if(isset($updates[$fields]))  {
                   $role->$fields = $updates[$fields];
               }
           }
       }
       return  $role->save();
   }

   public function addPermission($role_id,$permission_id) {
       $role = Role::find($role_id);
       $role->Permission()->attach($permission_id);
   }

   public function removePermission($role_id,$permission_id) {
       $role = Role::find($role_id);
       $role->Permission()->detach($permission_id);
   }



}