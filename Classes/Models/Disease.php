<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 6:48 PM
 */
namespace Models;
use Illuminate\Database\Eloquent\Model as Emodel;

class Disease extends EModel
{
    protected $fillable = ['label', 'description', 'status_id'];
    protected  $guarded = ['id'];

    public  function  Symptom() {
        return $this->belongsToMany('Models\Symptom');
    }

    public static function createDisease($data) {
        if(self::isDiseaseExisting($data['label'])) {
            return  self::isDiseaseExisting($data['label']);
        } else{
            $disease = new Disease($data);
            return ($disease->save()) ? $disease : null;
        }
    }

    public static function isDiseaseExisting($label) {
        $disease = Disease::where('label',$label)->first();
        return (isset($disease)) ? $disease : null;
    }

    public static function getDiseases($ids) {
        //return Disease::findMany($ids)->Symptom;
        return Disease::with('Symptom')->findMany($ids);
    }

    public static function allDiseases() {
        return Disease::with('Symptom')->orderBy('label', 'asc')->get();
    }

    public static function addSymptom($data) {
        $disease = Disease::find($data['disease_id']);
        if($disease) {
            $check =  $disease->Symptom()->where(['disease_id' => $data['disease_id'], 'symptom_id' => $data['symptom_id']])->first();
            if($check) {
                return $check;
            } else{
                $disease->Symptom()->attach($data['symptom_id'], $data);
                return $disease->Symptom()->where(['disease_id' => $data['disease_id'], 'symptom_id' => $data['symptom_id']])->first();
            }
        }

    }
}