<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 5:17 PM
 */

namespace Models;
use Illuminate\Database\Eloquent\Model as Emodel;

class Permission extends Emodel
{
    protected $fillable =['label', 'description', 'status_id'];
    protected $guarded =['id'];

   public function Role() {
       return $this->belongsToMany('Models\Role');
   }

    public function createPermission() {

    }

    public function editPermission() {

    }





}