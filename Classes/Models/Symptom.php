<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 6:47 PM
 */
namespace Models;
use Illuminate\Database\Eloquent\Model as Model;

class Symptom extends Model
{
    protected $fillable = ['label', 'description', 'status_id'];
    protected  $guarded = ['id'];

    public function Disease() {
        return $this->belongsToMany('Models\Disease');
    }

    public static function  createSymptom($data) {
        if(self::isSymptomExisting($data['label'])) {
           return  self::isSymptomExisting($data['label']);
        } else{
            $symptom = new Symptom($data);
            return ($symptom->save()) ? $symptom : null;
        }
    }

    public static function isSymptomExisting($label) {
        $symptom = Symptom::where('label',$label)->first();
        return (isset($symptom)) ? $symptom : null;
    }
}