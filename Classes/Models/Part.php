<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 5:01 PM
 */
namespace Models;
use Illuminate\Database\Eloquent\Model as Emodel;

class Part extends Emodel {
    protected $fillable = ['label', 'description', 'status_id'];
    protected $guarded = ['id'];

    public  function  Herb() {
        return $this->belongsToMany('Models\Herb');
    }

    public static function createPart($data) {
        $part = new Part($data);
        return ($part->save()) ? $part : null;
    }
}