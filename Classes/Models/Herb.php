<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 4:39 PM
 */
namespace Models;
use Illuminate\Database\Eloquent\Model as Model;

class Herb extends  Model
{
    protected $fillable =['name' , 'common_name', 'botanical_name', 'image_url', 'local_name', 'general_description', 'total_reccomendation', 'status_id'];
    protected $guarded =['id'];

    public function Part() {
        return $this->belongsToMany('Models\Part')->withPivot('image_url','disease_ids', 'status_id','id')->withTimestamps();
    }

    public static function createHerb($data) {
        $herb = new Herb($data);
        return ($herb->save()) ? $herb : null;
    }

    public  static  function getHerbs() {
       return Herb::with('Part')->get();
    }

    public static function getHerb($id) {
        return Herb::with('Part')->where(['id'=> $id])->first();
    }

    public static function addPart($data) {


        $herb = Herb::find($data['herb_id']);
        if($herb) {
            $check =  $herb->Part()->where(['herb_id' => $data['herb_id'], 'part_id' => $data['part_id']])->first();
            if($check) {
                return $check;
            } else{
                $herb->Part()->attach($data['part_id'], $data);
                return $herb->Part()->where(['herb_id' => $data['herb_id'], 'part_id' => $data['part_id']])->first();
            }
        }

    }

    public static function getHerbParts($disease_ids) {

    }





}
