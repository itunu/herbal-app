<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 5:13 PM
 */

namespace Models;
use Illuminate\Database\Eloquent\Model as Model;

class Admin extends Model
{
    protected $fillable =['email' , 'phone_number', 'auth_no', 'image_url', 'status_id'];
    protected $guarded =['id'];

    public function Role() {
        return $this->belongsToMany('Models\Role');
    }


    public  static  function addAdmin($admin) {
        $new_admin = new Admin($admin);
        $new_admin->save();
        return $new_admin;
    }

    public function editAdmin($updates) {
        $admin = Admin::find($updates['id']);
        $fields_to_update = ['email', 'phone_number','auth_no', 'image_url','status_id'];
        if(isset($admin)) {
            foreach($fields_to_update as $fields) {
                if(isset($updates[$fields]))  {
                    $admin->$fields = $updates[$fields];
                }
            }
        }
        return  $admin->save();
    }

    public static function assignRole($admin_id,$role_id) {
        $admin = Admin::find($admin_id);
         return $admin->Role()->attach($role_id);
    }

    public function removeRole($admin_id,$role_id){
        $admin = Admin::find($admin_id);
        $admin->Role()->detach($role_id);
    }

    public static function login($phone_number, $auth_no) {
        $admin = Admin::where(['phone_number'=> $phone_number, 'auth_no' => $auth_no])->first();
        if($admin) {
            return $admin;
        }
        return null;
    }

}
