<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 5:25 PM
 */
namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Models\Part;
use Utils\Utils;

class PartController extends BaseController
{
    public function addPart(Request $request, Response $response, $args) {
        try {
            $data = $request->getParsedBody();
            if($data) {
                $result = Part::createPart($data);
                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Part Created Succesfully',$result));
                }else {
                    return $response->withJson(Utils::setErrorMessage('105','Couldn\'t create part'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('105','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('101',$e->getMessage()));
        }
    }
}