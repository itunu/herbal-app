<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 6:48 PM
 */
namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Models\Symptom;
use Utils\Utils;

class SymptomController extends BaseController
{
    public function createSymptom(Request $request, Response $response, $args) {
        try {
            $data = $request->getParsedBody();
            if($data) {
                $result = Symptom::createSymptom($data);
                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Symptom Created Succesfully',$result));
                }else {
                    return $response->withJson(Utils::setErrorMessage('107','Couldn\'t create symptom'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('107','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('107',$e->getMessage()));
        }
    }
}