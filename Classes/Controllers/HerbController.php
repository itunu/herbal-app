<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 5:25 PM
 */

namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Models\Herb;
use Models\Disease;
use Models\Symptom;
use Models\Recipe;
use Utils\Utils;
use Imagick;

class HerbController extends  BaseController
{
    public function  createHerb(Request $request, Response $response, $args) {
        try {
            $data = $request->getParsedBody();
            if($data) {
                $data['local_name'] = json_encode($data['local_name']);
                $result = Herb::createHerb($data);

                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Herb Created Succesfully',$result));
                }else {
                    return $response->withJson(Utils::setErrorMessage('104','Couldn\'t create herb'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('104','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('104',$e->getMessage()));
        }
    }

    public function serveImage(Request $request, Response $response, $args) {
        try {
           $name = $args['name'];
            $dir = dirname(__DIR__)."/../uploads/";
            var_dump($dir.$name);
            $img  = file_get_contents($dir.$name);
            $response->write($img);
            return $response->withHeader('Content-Type', FILEINFO_MIME_TYPE);
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('104',$e->getMessage()));
        }
    }

    public function addPart(Request $request, Response $response, $args) {
        try {

            $data = $request->getParsedBody();
            $report = [
                'diseases' =>['total'=> 0, 'successful' => 0],
                'symptoms' => ['total' => 0, 'successful' => 0] ,
                'recipes' => ['total' => 0, 'successful' => 0],
                'parts' =>   ['total' => 0, 'successful' => 0]
            ];
            if($data) {
                $herb_id = $data['herb_id'];
                foreach ($data['parts'] as $part) {
                    $recipes =[];
                     $report['parts']['total']++;
                    $herb_part_data = ['herb_id'=> $herb_id, 'part_id'=>$part['part_id'], 'disease_ids'=>[]];
                    foreach ($part['diseases'] as $disease) {
                        if ($disease) {
                            $report['diseases']['total']++;
                            $new_disease = Disease::createDisease($disease);
                            ($new_disease) ? $report['diseases']['successful']++ && array_push($herb_part_data['disease_ids'], $new_disease['id']) : '';
                            $disease['recipes']['disease_id'] = $new_disease['id'];
                            array_push($recipes, $disease['recipes']);
                            if($disease['symptoms']) {
                                foreach ($disease['symptoms'] as $symptom) {
                                    $new_symptom = Symptom::createSymptom($symptom);
                                    $report['symptoms']['total']++;
                                    if($new_symptom) {
                                        $sym = [
                                            'symptom_id' => $new_symptom['id'],
                                            'disease_id' => $new_disease['id']
                                        ];
                                        Disease::addSymptom($sym);
                                        $report['symptoms']['successful']++;
                                    }
                                }
                            }
                        }
                    }
                    $herb_part_data['disease_ids'] = implode( ",", $herb_part_data['disease_ids']);
                    $herb_part = Herb::addPart($herb_part_data);
                    if($herb_part) {
                        $report['parts']['successful']++;
                        foreach ($recipes as $recipe) {
                            $recipe[0]['herb_part_id'] = $herb_part->pivot->id;
                            $recipe[0]['disease_id'] = $recipe['disease_id'];
                            $report['recipes']['total']++;
                            $new_recipe = Recipe::createRecipe($recipe[0]);
                            ($new_recipe) ? $report['recipes']['successful']++: '';
                        }
                    }
                }
               return $response->withJson(Utils::setSuccessMessage('Operation attempted succesfully', $report));
            } else {
                return $response->withJson(Utils::setErrorMessage('105','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('105',$e->getMessage()));
        }
    }

    public  function getHerb(Request $request, Response $response, $args) {
         try {
             $herbs = Herb::getHerbs();
             foreach ($herbs as $herb) {
                 foreach ($herb['part'] as $part) {
                     $disease = Disease::getDiseases(explode(",",$part->pivot->disease_ids));
                     foreach ($disease as $dis) {
                         $dis->recipe = Recipe::getRecipe($dis->id,$part->pivot->id);
                     }
                     $part->disease = $disease;
                 }
             }
             return $response->withJson(Utils::setSuccessMessage('Herbs gotten successfully', $herbs));

         } catch (\Exception $e) {
             return $response->withJson(Utils::setErrorMessage('106',$e->getMessage()));
         }
    }

    public function getOneHerb(Request $request, Response $response, $args) {
        try {
            $herb = Herb::getHerb($args['id']);
            foreach ($herb['part'] as $part) {
                $disease = Disease::getDiseases(explode(",",$part->pivot->disease_ids));
                foreach ($disease as $dis) {
                    $dis->recipe = Recipe::getRecipe($dis->id,$part->pivot->id);
                }
                $part->disease = $disease;
            }

            return $response->withJson(Utils::setSuccessMessage('Herb gotten successfully', $herb));

        } catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('106',$e->getMessage()));
        }
    }

    public function getHerbParts(Request $request, Response $response, $args) {
        try{
            $herb_id = $args['id'];
            $parts = Herb::getHerbParts($herb_id);
            return $response->withJson(Utils::setSuccessMessage('Parts gotten successfully', $parts));
        } catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('107',$e->getMessage()));
        }
    }
}