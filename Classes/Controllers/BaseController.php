<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 9/4/2017
 * Time: 4:28 PM
 */
namespace Controllers;
use \Interop\Container\ContainerInterface;
use Utils\Utils;
use Slim\Http\UploadedFile;

 abstract class BaseController
 {
   protected $container;

   public function __construct(ContainerInterface $c){
       $this->container = $c;
       if(Utils::isLoggedIn()) {
           //making the current admin data available to all controllers
           $this->admin = Utils::setSession('admin');
       }
   }

   public function upload($request) {
       $directory = $this->container->get('upload_directory');
       $uploadedFiles = $request->getUploadedFiles();

       // handle single input with single file upload
       $uploadedFile = $uploadedFiles['image'];
       if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
           $filename = moveUploadedFile($directory, $uploadedFile);
            return $filename;
       }
       return false;
   }
}