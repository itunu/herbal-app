<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 9/4/2017
 * Time: 4:27 PM
 */
namespace Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
 use Models\Role;
 use Utils\Utils;

class RoleController extends BaseController
{
    public function createRole(Request $request, Response $response) {
        try{
            $data = $request->getParsedBody();
            if($data) {
                $result = Role::createRole($data);
                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Role created  Succesfully',$result));
                } else {
                    return $response->withJson(Utils::setErrorMessage('103','Couldn\'t create role'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('103','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('103',$e->getMessage()));
        }
    }
}