<?php
/**
 * Created by PhpStorm.
 * User: itunu
 * Date: 9/29/17
 * Time: 6:48 PM
 */
namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Models\Disease;
use Models\Symptom;
use Models\Herb;
use Models\Recipe;
use Utils\Utils;

class DiseaseController extends  BaseController
{
    public function createDisease(Request $request, Response $response, $args) {
        try {
            $data = $request->getParsedBody();
            if($data) {
                $result = Disease::createDisease($data);
                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Disease Created Succesfully',$result));
                }else {
                    return $response->withJson(Utils::setErrorMessage('106','Couldn\'t create disease, probabaly exist before'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('106','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('106',$e->getMessage()));
        }
    }

    public function addSymptom(Request $request, Response $response, $args) {

        try {
            $data = $request->getParsedBody();
            if ($data) {
                $result = Disease::addSymptom($data);
                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Disease Created Succesfully',$result));
                }else {
                    return $response->withJson(Utils::setErrorMessage('106','Couldn\'t create disease, probabaly exist before'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('106','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('106',$e->getMessage()));
        }
    }

    public  function getDrriseases(Request $request, Response $response, $args) {
        try{
            $diseases = Disease::allDiseases();
            $herbs = Herb::getHerbs();
            if ($diseases) {
                foreach ($diseases as $disease) {
                    foreach ($herbs as $herb) {
                        $he = [];
                        $hb = [];
                        foreach ($herb['part'] as $part) {
                            $dar = [];
                            $disease_ids = $part->pivot->disease_ids;
                            $dar = explode(',',$disease_ids);
                            if(in_array($disease->id, $dar)) {
                                $part->recipe = Recipe::getRecipe($disease->id,$part->pivot->id);
                               array_push($he,$part);
                               array_push($hb,$herb);
                            }
                        }
                        }
                        $disease->herb_part = $he;
                        $disease->herbs = $hb;
                    }
                return $response->withJson(Utils::setSuccessMessage('Diseases fetched successfully', $diseases));
                }
                else {
                return $response->withJson(Utils::setErrorMessage('106','Couldn\'t fecth diseases'));
            }

        } catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('106',$e->getMessage()));
        }
    }

    public  function getDiseases(Request $request, Response $response, $args) {
        try {
            $diseases = Disease::allDiseases();
            $herbs = Herb::getHerbs();
            if($diseases) {
                foreach ($diseases as $disease) {
                    $disease = self::getDiseaseHerbs($disease);
                }
                return $response->withJson(Utils::setSuccessMessage('Diseases fetched successfully', $diseases));
            }
        }  catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('106',$e->getMessage()));
        }
    }

    public function getDiseaseHerbs($disease) {
        $herbs = Herb::getHerbs();
        $he = [];
        $hb = [];
        if($herbs) {
            foreach ( $herbs as $herb) {
                $herb->mypart = [];
                foreach ($herb['part'] as $part) {
                    $dar = [];
                    $disease_ids = $part->pivot->disease_ids;
                    $dar = explode(',',$disease_ids);
                    if(in_array($disease->id, $dar)) {
                        $part->recipe = Recipe::getRecipe($disease->id,$part->pivot->id);
                        array_push($hb,$herb);
                    }

                }

            }
            $disease->herbs = $hb;
           return $disease;
        }
        return [];
    }
}