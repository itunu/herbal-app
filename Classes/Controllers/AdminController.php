<?php
/**
 * Created by PhpStorm.
 * User: Babalola
 * Date: 8/31/2017
 * Time: 5:13 PM
 */

namespace Controllers;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Models\Admin;
use Utils\Utils;

class AdminController extends BaseController
{
    public  function addAdmin(Request $request, Response $response,$args) {
        try {
            $data = $request->getParsedBody();
            if($data) {
                $result = Admin::addAdmin($data);
                if($result) {
                    return  $response->withJson(Utils::setSuccessMessage('Admin Created Succesfully',$result));
                }else {
                    return $response->withJson(Utils::setErrorMessage('101','Couldn\'t create admin'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('101','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('101',$e->getMessage()));
        }
    }

    public function login(Request $request,Response $response,$args) {
        try{
            $data = $request->getParsedBody();
            if($data) {
                $result = Admin::login($data['phone_number'], $data['auth_no']);
                if($result) {
                    Utils::setSession('admin', $result);
                    Utils::registerSession();
                    return  $response->withJson(Utils::setSuccessMessage('Logged in Succesfully',$result));
                } else {
                    return $response->withJson(Utils::setErrorMessage('102','Invalid Login Creientials'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('102','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('102',$e->getMessage()));
        }

    }


    public function assignRole(Request $request, Response $response) {
        try{
            $data = $request->getParsedBody();
            if($data) {
                $result = Admin::assignRole($this->admin['id'], $data['role_id']);
                if(!$result) {
                    return  $response->withJson(Utils::setSuccessMessage('Role assigned successful',$result));
                } else {
                    return $response->withJson(Utils::setErrorMessage('104','Couldn\'t assign roles'));
                }
            } else {
                return $response->withJson(Utils::setErrorMessage('104','No data'));
            }
        }catch (\Exception $e) {
            return $response->withJson(Utils::setErrorMessage('104',$e->getMessage()));
        }
    }

}