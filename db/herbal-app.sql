-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2017 at 09:14 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `herbal-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `auth_no` varchar(255) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `phone_number`, `auth_no`, `image_url`, `created_at`, `updated_at`, `status_id`) VALUES
(1, 'oyinkansolaariyo@gmail.com', '07064310834', 'TMP101', NULL, '2017-09-28 18:08:14', '2017-09-28 18:08:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role`
--

CREATE TABLE `admin_role` (
  `admin_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_role`
--

INSERT INTO `admin_role` (`admin_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2017-09-28 20:23:32', '2017-09-28 20:23:32'),
(1, 1, '2017-09-28 20:27:12', '2017-09-28 20:27:12'),
(1, 2, '2017-09-28 20:28:12', '2017-09-28 20:28:12'),
(1, 2, '2017-09-28 20:28:38', '2017-09-28 20:28:38'),
(1, 2, '2017-09-28 20:28:40', '2017-09-28 20:28:40'),
(1, 2, '2017-09-28 20:28:52', '2017-09-28 20:28:52'),
(1, 2, '2017-09-28 20:29:13', '2017-09-28 20:29:13'),
(1, 2, '2017-09-28 20:29:17', '2017-09-28 20:29:17'),
(1, 2, '2017-09-28 20:29:18', '2017-09-28 20:29:18'),
(1, 2, '2017-09-28 20:32:18', '2017-09-28 20:32:18'),
(1, 1, '2017-09-28 20:32:25', '2017-09-28 20:32:25'),
(1, 1, '2017-09-28 20:32:27', '2017-09-28 20:32:27');

-- --------------------------------------------------------

--
-- Table structure for table `classifications`
--

CREATE TABLE `classifications` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `has_parts` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `diseases`
--

CREATE TABLE `diseases` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `diseases`
--

INSERT INTO `diseases` (`id`, `label`, `description`, `status_id`, `created_at`, `updated_at`) VALUES
(21, 'Stomach Ache', 'This refers to cramps or a dull ache in the tummy(abdomen)', 0, '2017-10-03 21:02:08', '2017-11-26 06:30:08'),
(22, 'Skin Infection', 'Skin Infections such as ringworm, rashes and ezcema', 0, '2017-10-03 21:02:08', '2017-10-03 21:02:08'),
(23, 'Diabetes', 'A group of diseases that result in too  much sugar in the blood (high blood glucose)', 0, '2017-10-03 21:02:08', '2017-11-26 06:25:08'),
(24, 'Loss of Memory', 'It might be a symptom of diabetes, Alzheimer\'s or another dementia. Alzheimer is a brain disease that causes a slow decline in memory, thinking and reasoning skills.', 0, '2017-10-03 21:02:08', '2017-11-26 06:36:15'),
(25, 'Postrate Cancer', 'A cancer in a mans postrate, a small walnut-sized gland that produces seminal fluid', 0, '2017-10-03 21:02:08', '2017-11-26 06:41:03'),
(26, 'General Weakness', 'Lack of vitality and vigour, constant tiredness', 0, '2017-10-03 21:02:08', '2017-10-03 21:02:08'),
(27, 'Stroke', 'This is a condition of damage to the brain from interruption of blood supply.', 0, '2017-10-03 21:02:08', '2017-11-26 06:43:11'),
(28, 'Pneumonia', 'Infection that inflames air sacs in one or both lungs, which may fill with fluid', 0, '2017-10-03 21:02:08', '2017-11-26 06:48:33'),
(29, 'Insomnia', 'Persistent problems falling and staying asleep', 0, '2017-10-03 21:02:08', '2017-11-26 06:51:30'),
(30, 'Arthritis', 'Inflammation of one or more joints, causing pain and stiffness that can worsen with age', 0, '2017-10-03 21:02:08', '2017-11-26 06:54:34'),
(31, 'Malaria', 'A disease caused by a plasmodium parasite, transmitted by the bite of infected mosquitoes', 0, '2017-11-12 13:24:06', '2017-11-26 06:58:27'),
(32, 'Convulsion', 'This is a seizure.It is a change in the brains electrical activity.', 0, '2017-11-12 13:24:06', '2017-11-26 07:00:12'),
(33, 'Asthma', 'A condition in which a person\'s airways become inflamed, narrow and swell and produce extra mucus, which makes it difficult to breathe ', 0, '2017-11-12 13:24:06', '2017-11-26 07:03:33'),
(34, 'Bronchitis', 'This is the inflammation of the lining of the bronchial tubes which carry air to and from the lungs', 0, '2017-11-12 13:26:06', '2017-11-26 07:07:12'),
(35, 'External Ulcer', 'This is an open wound on the skin. It is could be caused by a health problem such as infection, by a pressure sore, or by vein ulcers.', 0, '2017-11-12 13:26:06', '2017-11-26 07:11:59'),
(36, 'Stomach Ulcer', 'A sore that develops on the lining of the oesophagus, stomach or small intestine', 0, '2017-11-12 13:26:06', '2017-11-26 07:16:33'),
(37, 'Impotence', 'This occurs when a man cant get or keep an erection firm enough for sexual intercourse', 0, '2017-11-12 13:26:06', '2017-11-26 07:20:00'),
(38, 'Hepatitis', 'This is an inflammatory condition of the liver. It is commonly caused by a viral infection.', 0, '2017-11-12 13:57:05', '2017-11-26 07:23:27'),
(39, 'Fibroid', 'Non-cancerous growths in the uterus that can develop during a woman\'s childbearing years', 0, '2017-11-12 13:57:05', '2017-11-26 07:34:10'),
(40, 'Cancer', 'A disease in which abnormal cells divide uncontrollably and destroy body tissue.', 0, '2017-11-12 14:25:06', '2017-11-26 07:37:43'),
(41, 'Intestinal Cancer', 'A cancer of the colon or rectum, located at the digestive tract\'s lower end.', 0, '2017-11-12 14:25:07', '2017-11-26 07:38:48'),
(42, 'Suppressed Menstruation', 'Aloe vera is useful for all forms of gyneacolgical problem', 0, '2017-11-12 14:25:07', '2017-11-12 14:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `disease_symptom`
--

CREATE TABLE `disease_symptom` (
  `disease_id` int(11) NOT NULL,
  `symptom_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disease_symptom`
--

INSERT INTO `disease_symptom` (`disease_id`, `symptom_id`, `status_id`, `created_at`, `updated_at`) VALUES
(25, 3, 0, '2017-10-03 21:02:08', '2017-10-03 21:02:08'),
(31, 4, 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(31, 5, 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(31, 6, 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(31, 7, 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(23, 8, 0, '2017-11-26 06:28:44', '2017-11-26 06:28:44'),
(23, 9, 0, '2017-11-26 06:28:44', '2017-11-26 06:28:44'),
(22, 10, 0, '2017-11-26 06:33:45', '2017-11-26 06:33:45'),
(22, 11, 0, '2017-11-26 06:33:45', '2017-11-26 06:33:45'),
(24, 12, 0, '2017-11-26 06:39:17', '2017-11-26 06:39:17'),
(24, 13, 0, '2017-11-26 06:39:17', '2017-11-26 06:39:17'),
(25, 8, 0, '2017-11-26 06:41:53', '2017-11-26 06:41:53'),
(27, 14, 0, '2017-11-26 06:46:31', '2017-11-26 06:46:31'),
(27, 15, 0, '2017-11-26 06:46:31', '2017-11-26 06:46:31'),
(27, 16, 0, '2017-11-26 06:46:53', '2017-11-26 06:46:53'),
(27, 17, 0, '2017-11-26 06:46:53', '2017-11-26 06:46:53'),
(28, 18, 0, '2017-11-26 06:50:28', '2017-11-26 06:50:28'),
(28, 19, 0, '2017-11-26 06:50:28', '2017-11-26 06:50:28'),
(28, 20, 0, '2017-11-26 06:50:44', '2017-11-26 06:50:44'),
(28, 21, 0, '2017-11-26 06:50:44', '2017-11-26 06:50:44'),
(29, 22, 0, '2017-11-26 06:53:21', '2017-11-26 06:53:21'),
(29, 23, 0, '2017-11-26 06:53:21', '2017-11-26 06:53:21'),
(30, 24, 0, '2017-11-26 06:57:13', '2017-11-26 06:57:13'),
(32, 26, 0, '2017-11-26 07:01:55', '2017-11-26 07:01:55'),
(32, 27, 0, '2017-11-26 07:01:55', '2017-11-26 07:01:55'),
(33, 28, 0, '2017-11-26 07:06:02', '2017-11-26 07:06:02'),
(33, 29, 0, '2017-11-26 07:06:02', '2017-11-26 07:06:02'),
(34, 4, 0, '2017-11-26 07:08:57', '2017-11-26 07:08:57'),
(34, 29, 0, '2017-11-26 07:08:57', '2017-11-26 07:08:57'),
(34, 30, 0, '2017-11-26 07:09:08', '2017-11-26 07:09:08'),
(35, 31, 0, '2017-11-26 07:15:17', '2017-11-26 07:15:17'),
(35, 32, 0, '2017-11-26 07:15:17', '2017-11-26 07:15:17'),
(36, 33, 0, '2017-11-26 07:18:26', '2017-11-26 07:18:26'),
(36, 34, 0, '2017-11-26 07:18:26', '2017-11-26 07:18:26'),
(37, 35, 0, '2017-11-26 07:22:09', '2017-11-26 07:22:09'),
(37, 36, 0, '2017-11-26 07:22:09', '2017-11-26 07:22:09'),
(38, 37, 0, '2017-11-26 07:32:04', '2017-11-26 07:32:04'),
(38, 38, 0, '2017-11-26 07:32:04', '2017-11-26 07:32:04'),
(39, 39, 0, '2017-11-26 07:36:11', '2017-11-26 07:36:11'),
(39, 40, 0, '2017-11-26 07:36:11', '2017-11-26 07:36:11'),
(41, 41, 0, '2017-11-26 07:41:42', '2017-11-26 07:41:42'),
(41, 42, 0, '2017-11-26 07:41:42', '2017-11-26 07:41:42'),
(41, 43, 0, '2017-11-26 07:41:54', '2017-11-26 07:41:54'),
(42, 40, 0, '2017-11-26 07:43:51', '2017-11-26 07:43:51');

-- --------------------------------------------------------

--
-- Table structure for table `herbs`
--

CREATE TABLE `herbs` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `common_name` varchar(150) NOT NULL,
  `botanical_name` varchar(150) NOT NULL,
  `local_name` varchar(255) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `general_description` varchar(255) NOT NULL,
  `total_reccomendation` int(11) DEFAULT NULL,
  `classification_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `herbs`
--

INSERT INTO `herbs` (`id`, `name`, `common_name`, `botanical_name`, `local_name`, `image_url`, `general_description`, `total_reccomendation`, `classification_id`, `status_id`, `created_at`, `updated_at`) VALUES
(2, 'Bitter Leaf', 'Ewuro or Bitter Leaf', 'vernonia amydalina', '[{"language_id":1,"name":"Ewuro"},{"language_id":2,"name":"Onugbu"},{"language_id":3,"name":"Shiwaka"}]', 'ewuro.jpg', 'Bitter Leaf is a bitter herb , It helps to tone the vital organs of the body, especially the liver and the kidney', NULL, NULL, 0, '2017-10-03 21:01:26', '2017-11-12 21:37:57'),
(3, 'PawPaw', 'PawPaw', 'Carica Papaya', '[{"language_id":1,"name":"\\u00ecb\\u1eb9p\\u1eb9"},{"language_id":2,"name":"m\\u0301b\\u00ed-m\\u0301gb\\u00ed "},{"language_id":3,"name":"k\\u00e1w \\u00f9s\\u00e8"}]', 'pawpawtree.jpg', 'Pawpaw is a very good antioxidant, it metabolizes other foods we eat', NULL, NULL, 0, '2017-11-12 11:32:43', '2017-11-12 21:38:19'),
(4, 'Coconut', 'Coconut', 'Cocus Nucifera', '[{"language_id":1,"name":"\\u00e0gb\\u00f3n"},{"language_id":2,"name":"aku-oyibo"},{"language_id":3,"name":"kwarkwar"}]', 'coconuttree.jpg', 'Coconut is a very useful plant.All its part are very useful', NULL, NULL, 0, '2017-11-12 11:54:34', '2017-11-12 22:32:26'),
(5, 'Aloe vera', 'Aloe vera', 'Aloe barbadensis miller', '[{"language_id":1,"name":""},{"language_id":2,"name":""},{"language_id":3,"name":""}]', 'aloeveratree.jpg', 'It grows wild in tropical climates around the world and is cultivated for agricultural and medicinal uses', NULL, NULL, 0, '2017-11-12 12:25:28', '2017-11-12 22:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `herb_part`
--

CREATE TABLE `herb_part` (
  `id` int(11) NOT NULL,
  `herb_id` int(11) NOT NULL,
  `part_id` int(11) NOT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `disease_ids` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `herb_part`
--

INSERT INTO `herb_part` (`id`, `herb_id`, `part_id`, `image_url`, `disease_ids`, `status_id`, `created_at`, `updated_at`) VALUES
(14, 2, 5, 'leaf.jpg', '22,23,24,25,26,27,28,29,30', 0, '2017-10-03 21:02:09', '2017-11-12 21:39:04'),
(15, 2, 2, 'stem.jpg', '21', 0, '2017-10-03 21:14:59', '2017-11-12 21:39:26'),
(16, 3, 5, 'pawpawleave.jpg', '23,32,33,31', 0, '2017-11-12 13:24:06', '2017-11-13 05:17:08'),
(17, 3, 1, 'pawpawroot.jpg', '34', 0, '2017-11-12 13:26:06', '2017-11-12 21:40:24'),
(18, 3, 4, 'pawpawfruit.jpg', '35,36,37', 0, '2017-11-12 13:26:06', '2017-11-12 21:41:06'),
(19, 4, 1, 'coconutroot.jpg', '38,39', 0, '2017-11-12 13:57:05', '2017-11-12 22:33:30'),
(20, 5, 5, 'aloeveratree.jpg', '41,42', 0, '2017-11-12 14:25:07', '2017-11-12 22:33:47'),
(21, 5, 1, 'aloeveraroot.jpg', '37', 0, '2017-11-12 14:25:07', '2017-11-12 22:34:04');

-- --------------------------------------------------------

--
-- Table structure for table `parts`
--

CREATE TABLE `parts` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(120) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parts`
--

INSERT INTO `parts` (`id`, `label`, `description`, `created_at`, `updated_at`, `status_id`) VALUES
(1, 'ROOT', 'This is the part of the plant that is in the ground', '2017-09-29 17:28:19', '2017-09-29 17:28:19', 0),
(2, 'STEM', 'This is the part of the plant that connects the leaves and the root', '2017-09-29 17:28:58', '2017-11-18 13:25:44', 0),
(4, 'FRUIT', 'These are the fruits of the medicinal plant', '2017-09-29 17:29:38', '2017-11-18 13:23:42', 0),
(5, 'LEAVES', 'These  are the leaves of the medicinal plant', '2017-09-29 17:30:06', '2017-11-18 13:24:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `recipes`
--

CREATE TABLE `recipes` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `herb_part_id` int(11) NOT NULL,
  `disease_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipes`
--

INSERT INTO `recipes` (`id`, `label`, `description`, `herb_part_id`, `disease_id`, `status_id`, `created_at`, `updated_at`) VALUES
(23, 'Bitter Leaf Remedy for Stomach Aches', '{\r\n	"Preparation":"Pluck fresh  green healthy bitter leaves in the evening.Pound the leaves in a mortar and press out the juice. Add a pinch of salt to three tablespoonfuls of the undiluted juice",\r\n	"Dosage":"Drink the resulting mixture once and for all, This brings immediate relief.",\r\n	"Taboo":"None."\r\n}', 14, 21, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:35'),
(24, 'Bitter Leaf Remedy for Skin Infection', '{\r\n	"Preparation":"Pluck fresh  green healthy bitter leaves in the evening.Pound the leaves in a mortar and press out the juice. ",\r\n	"Dosage":"Apply the  undiluted juice on the affected part daily until the Skin infection goes totally.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 22, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:35'),
(25, 'Bitter Leaf Remedy for Diabetes', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in ten liters of clean water. ",\r\n	"Dosage":"Take two glasses three times daily for one month.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 23, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(26, 'Bitter Leaf Remedy for Loss of memory', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in ten liters of clean water. ",\r\n	"Dosage":"Take one glasses three times daily for two  months.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 24, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(27, 'Bitter Leaf Remedy for Postrate Cancer', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in ten liters of clean water.Bitter leaf increases the flow of urine and reduces the pain , as well as regulates the spread of the cell. ",\r\n	"Dosage":"Take two glasses three times daily for one  month.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 25, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(28, 'Bitter Leaf Remedy for General Weakness', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in a liter of clean water.",\r\n	"Dosage":"Take a glass three times daily until you stop feeling weak.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 26, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(29, 'Bitter Leaf Remedy for Stroke', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in a liter of clean water.Bitter leaf solution calms the nerves, strengthens the muscles and cleanses the system.",\r\n	"Dosage":"Take a glass three times daily until the stroke goes.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 27, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(30, 'Bitter Leaf Remedy for Pneumonia', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in a liter of clean water.Bitter leaf solution calms the nerves, strengthens the muscles and cleanses the system.",\r\n	"Dosage":"Take a glass three times daily until the Pneumonia stops.",\r\n	"Taboo":"WARM not BOIL  the solution on fire each time before drinking.Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 28, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(31, 'Bitter Leaf Remedy for Insomnia', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in a liter of clean water.You may add a little honey if you wish",\r\n	"Dosage":"Take a glass three times daily until the Insomnia stops.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 29, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(32, 'Bitter Leaf Remedy for Arthritis', '{\r\n	"Preparation":"Pluck  ten  handfuls of fresh  green healthy bitter leaves in the evening.Squeeze the leaves in a liter of clean water.It soothes inflamed joints and eradicates the pain",\r\n	"Dosage":"Take a glass three times daily until the Arthritis stops.",\r\n	"Taboo":"Do not pluck the leaves very early in the morning or very late at night."\r\n}', 14, 30, 0, '2017-10-03 21:02:09', '2017-11-14 15:19:36'),
(33, '', '{\r\n	"Preparation":"Cut the tender stem of the plant like a chewing stick in the evening",\r\n	"Dosage":"Chew the stem like a bitter-leaf and swallow the bitterness.",\r\n	"Taboo":"Do not cut the stem very early in the morning or very late at night."\r\n}', 15, 21, 0, '2017-10-03 21:14:59', '2017-11-14 15:19:36'),
(34, 'PawPaw Remedy for Malaria', '{\r\n	"Preparation":"Pluck some yellow healthy pawpaw leaves in the evening.Squeeze the yellow leaves in a liter of water",\r\n	"Dosage":"Take a glassful of this mixture three times daily for seven days.",\r\n	"Taboo":"Do not cut the leaves very early in the morning or very late at night."\r\n}', 16, 31, 0, '2017-11-12 13:24:07', '2017-11-14 15:19:36'),
(35, 'PawPaw Remedy for Diabetes', '{\r\n	"Preparation":"Pluck some green healthy pawpaw leaves in the evening.Squeeze the green leaves in a liter of water",\r\n	"Dosage":"Take a glassful of this mixture three times daily until the system stops.",\r\n	"Taboo":"Do not cut the leaves very early in the morning or very late at night."\r\n}', 16, 23, 0, '2017-11-12 13:24:07', '2017-11-14 15:19:36'),
(36, 'PawPaw Remedy for Convulsion', '{\r\n	"Preparation":"Pickup dry fallen pawpaw leaves in the evening.Grind it to powder. Add two tablespoonfuls of the powder to half a glass cup of palm kernel oil.Stir the mixture well",\r\n	"Dosage":"Rub this mixture all over the body of the convulsed person.",\r\n	"Taboo":"Do not drink this mixture.Do not pick the leaves very early in the morning or very late at night."\r\n}', 16, 32, 0, '2017-11-12 13:26:06', '2017-11-14 15:19:36'),
(37, 'PawPaw remedy for Asthma', '{\r\n	"Preparation":"Pickup dry fallen pawpaw leaves in the evening.Burn it",\r\n	"Dosage":"Inhale the smoke from burning the leaves during an asthma attack,To prevent an attack inhale the smoke every night.",\r\n	"Taboo":"None "\r\n}', 16, 33, 0, '2017-11-12 13:26:06', '2017-11-14 15:19:36'),
(38, 'Paw Paw remedy for Bronchitis', '{\r\n	"Preparation":"Get fresh pawpaw roots, in the evening.Boil it",\r\n	"Dosage":"Take half a glass cup three times daily until the problem stops.",\r\n	"Taboo":"None "\r\n}', 17, 34, 0, '2017-11-12 13:26:06', '2017-11-14 15:19:36'),
(39, 'PawPaw Remedy for External Ulcer', '{\r\n	"Preparation":"The white milky sap of the unripe pawpaw contains a high percentage of papain, which is used for chronic wounds or ulcers.Make a slight cut on the unripe pawpaw fruit to allow the juice to drop.Boil it",\r\n	"Dosage":"Apply this juice to the wound or ulcer daily until the it dries up.",\r\n	"Taboo":"None "\r\n}', 18, 35, 0, '2017-11-12 13:26:06', '2017-11-14 15:19:36'),
(40, 'Paw Paw remedy for stomach ulcer', '{\r\n	"Preparation":"Cut a big unripe pawpaw fruit into pieces,Do not remove the peel or seeds.Simply cut the whole fruit into cubes.Then soak it in five bottles of water for  four days. Seive  the mixture",\r\n	"Dosage":"Take half a glass cup three times daily for two weeks.",\r\n	"Taboo":"None "\r\n}', 18, 36, 0, '2017-11-12 13:26:06', '2017-11-14 15:19:36'),
(41, 'Paw Paw remedy for Impotence', '{\r\n	"Preparation":"Cut two big unripe pawpaw fruit into pieces,Do not remove the peel or seeds.Simply cut the whole fruit into cubes.Boil it in eight bottles of water.",\r\n	"Dosage":"Take half a glass cup three times daily until you see positive changes.",\r\n	"Taboo":"None "\r\n}', 18, 37, 0, '2017-11-12 13:26:06', '2017-11-14 15:19:36'),
(42, 'Coconut remedy for Bronchitis', '{\r\n	"Preparation":"Get fresh  coconut and pawpaw roots in the evening.Chop an equal amount of coconut and pawpaw roots into pieces. Measure 10 handfuls of each into 10 litres of water. Add 5 bulbs of garlic into the mixtures. Boil the mixture together.Allow it to cool and mix with one bottle of honey.",\r\n	"Dosage":"Drink half of a glass cup three times daily until the problem stops.",\r\n	"Taboo":"None "\r\n}', 19, 34, 0, '2017-11-12 13:57:05', '2017-11-14 15:10:39'),
(43, 'Coconut remedy for Hepatitis', '{\r\n	"Preparation":"Grind the dried coconut root into powder. Mix 8 tablespoons of the powdered coconut root, 8 tablesspoons of powdered bitterkola. Add 1 botle of honey to the mixture.",\r\n	"Dosage":"Take 2 tablespoons three times daily until the problem stops.",\r\n	"Taboo":"None "\r\n}', 19, 38, 0, '2017-11-12 13:57:05', '2017-11-14 15:11:31'),
(44, 'Coconut remedy for Fibroid', '{\r\n	"Preparation":"Cut the fresh coconut root into pieces.Measure out fifteen handfuls of the pieces into ten bottles of water. Add 5 handful of xylopia aethiopica called uda in igbo , Erunje in Yoruba. Boil the mixture and allow it to stand for 24 hours.",\r\n	"Dosage":"Take half glass three times daily until the Firbroid melts away.",\r\n	"Taboo":"None "\r\n}', 19, 39, 0, '2017-11-12 13:57:05', '2017-11-14 15:12:49'),
(45, 'Aloe vera remedy for Cancer', '{\r\n	"Preparation":"Cut  3 fresh aloe vera leaves into pieces. Grind it together with 1 bottle of honey, and half bottle of dry gin.",\r\n	"Dosage":"Take 3 tablespoonful twice daily for 2 months.",\r\n	"Taboo":"None "\r\n}', 20, 40, 0, '2017-11-12 14:25:07', '2017-11-14 15:13:49'),
(46, 'Aloe vera remedy for Cancer', '{\r\n	"Preparation":"Cut  3 fresh aloe vera leaves into pieces. Grind it together with 1 bottle of honey, and half bottle of dry gin.",\r\n	"Dosage":"Take 2 tablespoonful twice daily for 2 months.",\r\n	"Taboo":"None "\r\n}', 20, 41, 0, '2017-11-12 14:25:07', '2017-11-14 15:14:37'),
(47, 'Aloe vera remedy for Suppressed menstruationr', '{\r\n	"Preparation":"Cut  4 fresh aloe vera leaves. Grind it together with 1 beer  bottle of honey, and half beer bottle of water. Add 4 tablespoons of dried, powdered ginger and mix it together.",\r\n	"Dosage":"Take 3 tablespoonful thrice daily for 1 month.",\r\n	"Taboo":"None "\r\n}', 20, 42, 0, '2017-11-12 14:25:07', '2017-11-14 15:15:42'),
(48, 'Aloe vera remedy for Impotencer', 'Cut fresh aloevera roots into pieces. The older the plant the more potent it is. Soak two handfuls of the root in one beer bottle of dry gin for ten days. Endeavour to shake the bottle each of those ten days. The dosage is six tablespoonfuls twice or thrice daily , depending on your need. However do not exceeed six table spoonfuls thrice daily', 21, 37, 0, '2017-11-12 14:25:07', '2017-11-12 14:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `label`, `description`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'The system admin', 0, '2017-09-28 19:12:11', '2017-09-28 19:12:11'),
(2, 'COMMON', 'Just a common user', 0, '2017-09-28 19:12:50', '2017-09-28 19:12:50');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `symptoms`
--

CREATE TABLE `symptoms` (
  `id` int(11) NOT NULL,
  `label` varchar(120) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `symptoms`
--

INSERT INTO `symptoms` (`id`, `label`, `description`, `status_id`, `created_at`, `updated_at`) VALUES
(3, 'Difficult and Painful Urination', 'This is when urinating becomes very painful and difficult', 0, '2017-10-03 21:02:08', '2017-10-03 21:02:08'),
(4, 'Headache', 'This is when the head aches', 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(5, 'High Temperature', 'This is when the temperature of the body is above normal', 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(6, 'Body Pain', 'This is when the body aches', 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(7, 'Bitter mouth', 'This is when the mouth tastes bitter', 0, '2017-11-12 13:24:06', '2017-11-12 13:24:06'),
(8, 'Frequent Urination', 'This is when you urinate frequently', 0, '2017-11-26 06:28:03', '2017-11-26 06:28:03'),
(9, 'Increased thirst and hunger', 'This is when you get hungry and thirsty more often', 0, '2017-11-26 06:28:03', '2017-11-26 06:28:03'),
(10, 'Redness of the skin', 'This is when the skin is very red', 0, '2017-11-26 06:33:14', '2017-11-26 06:33:14'),
(11, 'Itching', 'This is when the skin itches', 0, '2017-11-26 06:33:14', '2017-11-26 06:33:14'),
(12, 'Confusion with time, familiar places and familiar people', 'When the patient can no longer recognise places or people', 0, '2017-11-26 06:38:45', '2017-11-26 06:38:45'),
(13, 'Inability to perform regular tasks', 'When the patient can\'t recognise what he/she does regularly', 0, '2017-11-26 06:38:45', '2017-11-26 06:38:45'),
(14, 'Difficulty walking or speaking', '', 0, '2017-11-26 06:44:56', '2017-11-26 06:44:56'),
(15, 'Fatigue or Vertigo', '', 0, '2017-11-26 06:44:56', '2017-11-26 06:44:56'),
(16, 'Paralysis of one side of the body', '', 0, '2017-11-26 06:46:00', '2017-11-26 06:46:00'),
(17, 'Reduced sensation of touch', '', 0, '2017-11-26 06:46:00', '2017-11-26 06:46:00'),
(18, 'Fast or shallow breathing', '', 0, '2017-11-26 06:49:23', '2017-11-26 06:49:23'),
(19, 'Coughing', '', 0, '2017-11-26 06:49:23', '2017-11-26 06:49:23'),
(20, 'Sharp pain in the chest', '', 0, '2017-11-26 06:49:53', '2017-11-26 06:49:53'),
(21, 'Dehydration', '', 0, '2017-11-26 06:49:53', '2017-11-26 06:49:53'),
(22, 'Lack of concentration or slowness in activities', '', 0, '2017-11-26 06:52:53', '2017-11-26 06:52:53'),
(23, 'Depression', '', 0, '2017-11-26 06:52:53', '2017-11-26 06:52:53'),
(24, 'Swelling, Tenderness,Stiffness of the joints', '', 0, '2017-11-26 06:56:31', '2017-11-26 06:56:31'),
(26, 'Losing consciousness always followed by confusion', '', 0, '2017-11-26 07:01:23', '2017-11-26 07:01:23'),
(27, 'Drooling and frothing at the mouth', '', 0, '2017-11-26 07:01:23', '2017-11-26 07:01:23'),
(28, 'Difficulty in breathing, Shortness of breathe', '', 0, '2017-11-26 07:05:15', '2017-11-26 07:05:15'),
(29, 'Chest tightness, Throat irritation', '', 0, '2017-11-26 07:05:15', '2017-11-26 07:05:15'),
(30, 'Runny nose or post-nasal drip', '', 0, '2017-11-26 07:08:01', '2017-11-26 07:08:01'),
(31, 'Pain on the skin around the ulcer', '', 0, '2017-11-26 07:14:35', '2017-11-26 07:14:35'),
(32, 'Bloody, serous , purulent discharges from the ulcer', '', 0, '2017-11-26 07:14:35', '2017-11-26 07:14:35'),
(33, 'Heartburn', '', 0, '2017-11-26 07:17:45', '2017-11-26 07:17:45'),
(34, 'Nausea and vomiting', '', 0, '2017-11-26 07:17:45', '2017-11-26 07:17:45'),
(35, 'Sexual dysfunction or reduced sexual drive', '', 0, '2017-11-26 07:21:13', '2017-11-26 07:21:13'),
(36, 'Anxiety', '', 0, '2017-11-26 07:21:13', '2017-11-26 07:21:13'),
(37, 'Yellow skin or eyes', '', 0, '2017-11-26 07:31:22', '2017-11-26 07:31:22'),
(38, 'Dark urine or Pale stool', '', 0, '2017-11-26 07:31:22', '2017-11-26 07:31:22'),
(39, 'Pain in the abdomen, lower back or pelvis', '', 0, '2017-11-26 07:35:29', '2017-11-26 07:35:29'),
(40, 'Abnormal menstruation', '', 0, '2017-11-26 07:35:29', '2017-11-26 07:35:29'),
(41, 'Blood in stool', '', 0, '2017-11-26 07:40:44', '2017-11-26 07:40:44'),
(42, 'Anaemia or fatigue', '', 0, '2017-11-26 07:40:44', '2017-11-26 07:40:44'),
(43, 'Passing excessive amounts of gas', '', 0, '2017-11-26 07:41:09', '2017-11-26 07:41:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`,`phone_number`);

--
-- Indexes for table `classifications`
--
ALTER TABLE `classifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diseases`
--
ALTER TABLE `diseases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- Indexes for table `herbs`
--
ALTER TABLE `herbs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `herb_part`
--
ALTER TABLE `herb_part`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parts`
--
ALTER TABLE `parts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- Indexes for table `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `symptoms`
--
ALTER TABLE `symptoms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `classifications`
--
ALTER TABLE `classifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `diseases`
--
ALTER TABLE `diseases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `herbs`
--
ALTER TABLE `herbs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `herb_part`
--
ALTER TABLE `herb_part`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `parts`
--
ALTER TABLE `parts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `symptoms`
--
ALTER TABLE `symptoms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
