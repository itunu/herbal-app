<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        //database configuration
        'db' => [
            'driver' =>'mysql',
            'host' => 'us-cdbr-iron-east-05.cleardb.net',
            'database' =>'heroku_aa1e144922c511c',
            'username' => 'b2b2cc0a1d76a5',
            'password' => 'c283f648',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ]
    ],
];
