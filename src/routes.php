<?php
// Routes
$app->get('/[{name}]', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/' route");
    // Render index view
 return $this->renderer->render($response, '../public/admin/views/index.phtml', $args);
});
$app->options('/api/{routes:.+}', function ($request, $response, $args) {
    return $response;
});



$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', 'http://localhost:8100')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->post('/login','\Controllers\AdminController:login');

$app->group('/api',function() use ($app, $container){
    $app->post('/assign', '\Controllers\AdminController:assignRole');
    $app->post('/parts', '\Controllers\PartController:addPart');
    $app->post('/herbs', '\Controllers\HerbController:createHerb');
    $app->post('/herbs/part', '\Controllers\HerbController:addPart');
    $app->post('/add_admin', '\Controllers\AdminController:addAdmin');
    $app->post('/roles','\Controllers\RoleController:createRole');
    $app->get('/herbs', '\Controllers\HerbController:getHerb');
    $app->get('/diseases', '\Controllers\DiseaseController:getDiseases');
    $app->get('/herb/{id}/parts', '\Controllers\HerbController:getHerbParts');
    $app->get('/herb/{id}', '\Controllers\HerbController:getOneHerb');
    $app->get('/uploads/{name}', '\Controllers\HerbController:serveImage');


});