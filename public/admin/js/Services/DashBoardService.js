
'use strict';
!function() {
    var dashboardService = angular.module('HostelApp.services');
    dashboardService.service('DashBoardService',['$resource','$rootScope', 'StorageService',function ($resource,$rootScope,StorageService) {
        var token = StorageService.setDB('current_user').getRecord('data').token;
        var req = $resource('/api/:action/:id', {},{
            getHerbs: {method:'GET', headers:{'Authorization':token}, isArray:false, params:{action:'herbs'}},
            getHerb: {method:'GET', headers:{'Authorization':token}, isArray:false, params:{action:'herbs', id:'@id'}},
            addTMP: {method:'POST', headers:{'Authorization':token}, isArray:false, params:{action:'herbs'}},
        });


        return{
            getHerbs:getHerbs,
            getHerb:getHerb,
            addTMP:addTMP,

        };


        function getHerbs() {
            return  req.getHerbs().$promise;
        }
        function getHerb(params) {
            return  req.getHerbs(params).$promise;
        }


        function addTMP(params) {
            return req.addTMP(params).$promise;
        }


    }]);
}();