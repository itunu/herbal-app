'use strict';
!function() {
    var storageService = angular.module('HostelApp.services');
    storageService.service('StorageService', ['$rootScope', function ($rootScope) {
        var __local_storage = localStorage;


        return {
            createDB:createDB,
            setDB:setDB
        };

        function createDB(db_name) {
            if(__local_storage) {
                if(!__local_storage.getItem(db_name)) {
                    __local_storage.setItem(db_name, JSON.stringify({}));
                }
            }
        }

        function setDB(db_name) {
            createDB(db_name);
            return {
                saveRecord:saveRecord,
                getRecord:getRecord,
                hasRecord: hasRecord
            };

            function saveRecord(data_key, data) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        jRecord[data_key] = data;
                        __local_storage.setItem(db_name, JSON.stringify(jRecord));
                    }catch (e) {}
                }
            }

            function getRecord(key) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        if(key in jRecord) { return jRecord[key];}
                    }catch (e) {}
                }
                return false;
            }

            function hasRecord() {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        return Object.keys(jRecord).length > 0;
                    }catch (e) {
                        return false;
                    }
                }
                return false;
            }
        }

    }]);
}();