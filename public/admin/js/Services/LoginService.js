
'use strict';
!function() {
    var loginService = angular.module('HostelApp.services');
    loginService.service('LoginService',['$resource','$rootScope',function ($resource,$rootScope) {
        var req = $resource('/:action', {},{
            login: {method:'POST',isArray:false, params:{action:'login'}}
        });


        return{
            login:login
        };


        function login(params) {
            return  req.login(params).$promise;
        }
    }]);
}();