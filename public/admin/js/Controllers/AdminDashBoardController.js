'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('AdminDashBoardController',['$timeout','$scope','$window','ResponseService','StorageService','DashBoardService', function ($timeout,$scope,$window,ResponseService,StorageService,DashBoardService) {
        var vm = this;
        vm.active_user = StorageService.setDB('current_user').getRecord('data');
        vm.open_sidebar = false;
        vm.add = false;
        vm.toggleSidebar= toggleSidebar;
        vm.addTMP = addTMP;
        vm.herbs = [];
        vm.new_tmp = {
          name:'',
          common_name:'',
          botanical_name:'',
          local_name:[
              {
                  language_id:1,
                  name:''
              },
              {
                  language_id:2,
                  name:''
              },
              {
                  language_id:3,
                  name:""
              }
          ],
          general_description:''
        };


        ($timeout(getHerbs(), 5));

        function addTMP() {
            DashBoardService.addTMP(vm.new_tmp).then(function (response) {
                console.log(response);
            })
        }

        vm.showNew = function () {
          vm.add = true;
        };

        vm.showList = function () {
            vm.add = false;
        };

        vm.viewHerb = function (herb_id) {
            $window.location.href = '/herb_details/'+herb_id;
        };

        function getHerbs() {
            DashBoardService.getHerbs().then(function (response) {
              vm.herbs = response.data.response;
            })
        }


        function toggleSidebar() {
            console.log('hi');
            vm.open_sidebar = !vm.open_sidebar;
            console.log(vm.open_sidebar);
        }




    }]);
}();