'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('HerbController',['$scope','$window','$routeParams','ResponseService','StorageService','DashBoardService',function ($scope,$window,$routeParams,ResponseService,StorageService, DashBoardService) {
        var vm = this;
        vm.active_user = StorageService.setDB('current_user').getRecord('data');
        vm.open_sidebar = false;
        vm.add = false;
        console.log($routeParams);
        vm.herb_id = $routeParams.herb_id;
        vm.toggleSidebar= toggleSidebar;
        vm.addTMP = addTMP;
        vm.herb = {};


      /*  ($timeout(getHerb(), 5));*/

        function addTMP() {
            DashBoardService.addTMP(vm.new_tmp).then(function (response) {
                console.log(response);
            })
        }

        function getHerb() {
            DashBoardService.getHerb({id:vm.herb_id}).then(function (response) {
                vm.herb = response.data.response;
            })
        }


        function toggleSidebar() {
            console.log('hi');
            vm.open_sidebar = !vm.open_sidebar;
            console.log(vm.open_sidebar);
        }



    }]);
}();