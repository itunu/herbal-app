'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('LoginController',['$scope','$window','LoginService','ResponseService','StorageService',function ($scope,$window,LoginService,ResponseService,StorageService) {
        var vm = this;
        console.log(vm);
        vm.login_details = {
          'phone_number':'',
          'auth_no':''
        };
        vm.login = login;

        function login() {
            LoginService.login(vm.login_details).then(function (response) {
                console.log(response);
                var resp = ResponseService.Brew(response);
                if(response.status == 'success') {
                    console.log('hi');
                    StorageService.setDB('current_user').saveRecord('data', response.data.response);
                    $window.location.href = '/admin_dashboard';
                }
            })
        }
    }]);
}();