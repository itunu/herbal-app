'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('VendorDashBoardController',['$timeout','$scope','$window','ResponseService','StorageService','DashBoardService', function ($timeout,$scope,$window,ResponseService,StorageService,DashBoardService) {
        var vm = this;
        vm.active_user = StorageService.setDB('current_user').getRecord('data');
        vm.areas = [];
        vm.states = [];
        vm.categories = [];
        vm.open_sidebar = false;
        vm.vendors =[];
        vm.new_house = {};
        vm.getOwnerHouses = getOwnerHouses;
        vm.toggleSidebar= toggleSidebar;
        vm.viewVendor = viewVendor;

        $timeout(function () {
            getAreas();
            getOwnerHouses();
        },5);

        function viewVendor(id) {
            $window.location.href = '/vendor/'+id;
        }

        function toggleSidebar() {
            vm.open_sidebar = !vm.open_sidebar;
        }

        function getAreas() {
            DashBoardService.getAreas().then(function (response) {
                console.log(response);
            });
        }

        function getCategories() {
            DashBoardService.getCategories().then(function (response) {

            });
        }

        function getOwnerHouses() {
            DashBoardService.getOwnerHouses().then(function (response) {
                console.log(response);
            })
        }

        function getStates() {
            DashBoardService.getStates().then(function (response) {

            })
        }

        function addHouse() {
            DashBoardService.addHouse(vm.new_house).then(function (response) {
                console.log(response);
            })
        }






    }]);
}();