'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('AdminDashBoardController',['$timeout','$scope','$window','ResponseService','StorageService','DashBoardService', function ($timeout,$scope,$window,ResponseService,StorageService,DashBoardService) {
        var vm = this;
        vm.active_user = StorageService.setDB('current_user').getRecord('data');
        vm.open_sidebar = false;
        vm.add = false;
        vm.toggleSidebar= toggleSidebar;
        vm.addTMP = addTMP;
        vm.herbs = [];
        vm.new_tmp = {
          name:'',
          common_name:'',
          botanical_name:'',
          local_name:[
              {
                  language_id:1,
                  name:''
              },
              {
                  language_id:2,
                  name:''
              },
              {
                  language_id:3,
                  name:""
              }
          ],
          general_description:''
        };


        ($timeout(getHerbs(), 5));

        function addTMP() {
            DashBoardService.addTMP(vm.new_tmp).then(function (response) {
                console.log(response);
            })
        }

        vm.showNew = function () {
          vm.add = true;
        };

        vm.showList = function () {
            vm.add = false;
        };

        vm.viewHerb = function (herb_id) {
            $window.location.href = '/herb_details/'+herb_id;
        };

        function getHerbs() {
            DashBoardService.getHerbs().then(function (response) {
              vm.herbs = response.data.response;
            })
        }


        function toggleSidebar() {
            console.log('hi');
            vm.open_sidebar = !vm.open_sidebar;
            console.log(vm.open_sidebar);
        }




    }]);
}();
'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('HerbController',['$scope','$window','$routeParams','ResponseService','StorageService','DashBoardService',function ($scope,$window,$routeParams,ResponseService,StorageService, DashBoardService) {
        var vm = this;
        vm.active_user = StorageService.setDB('current_user').getRecord('data');
        vm.open_sidebar = false;
        vm.add = false;
        console.log($routeParams);
        vm.herb_id = $routeParams.herb_id;
        vm.toggleSidebar= toggleSidebar;
        vm.addTMP = addTMP;
        vm.herb = {};


      /*  ($timeout(getHerb(), 5));*/

        function addTMP() {
            DashBoardService.addTMP(vm.new_tmp).then(function (response) {
                console.log(response);
            })
        }

        function getHerb() {
            DashBoardService.getHerb({id:vm.herb_id}).then(function (response) {
                vm.herb = response.data.response;
            })
        }


        function toggleSidebar() {
            console.log('hi');
            vm.open_sidebar = !vm.open_sidebar;
            console.log(vm.open_sidebar);
        }



    }]);
}();
'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('LoginController',['$scope','$window','LoginService','ResponseService','StorageService',function ($scope,$window,LoginService,ResponseService,StorageService) {
        var vm = this;
        console.log(vm);
        vm.login_details = {
          'phone_number':'',
          'auth_no':''
        };
        vm.login = login;

        function login() {
            LoginService.login(vm.login_details).then(function (response) {
                console.log(response);
                var resp = ResponseService.Brew(response);
                if(response.status == 'success') {
                    console.log('hi');
                    StorageService.setDB('current_user').saveRecord('data', response.data.response);
                    $window.location.href = '/admin_dashboard';
                }
            })
        }
    }]);
}();
'use strict';
!function () {
    var app = angular.module('HostelApp.controllers');
    app.controller('VendorDashBoardController',['$timeout','$scope','$window','ResponseService','StorageService','DashBoardService', function ($timeout,$scope,$window,ResponseService,StorageService,DashBoardService) {
        var vm = this;
        vm.active_user = StorageService.setDB('current_user').getRecord('data');
        vm.areas = [];
        vm.states = [];
        vm.categories = [];
        vm.open_sidebar = false;
        vm.vendors =[];
        vm.new_house = {};
        vm.getOwnerHouses = getOwnerHouses;
        vm.toggleSidebar= toggleSidebar;
        vm.viewVendor = viewVendor;

        $timeout(function () {
            getAreas();
            getOwnerHouses();
        },5);

        function viewVendor(id) {
            $window.location.href = '/vendor/'+id;
        }

        function toggleSidebar() {
            vm.open_sidebar = !vm.open_sidebar;
        }

        function getAreas() {
            DashBoardService.getAreas().then(function (response) {
                console.log(response);
            });
        }

        function getCategories() {
            DashBoardService.getCategories().then(function (response) {

            });
        }

        function getOwnerHouses() {
            DashBoardService.getOwnerHouses().then(function (response) {
                console.log(response);
            })
        }

        function getStates() {
            DashBoardService.getStates().then(function (response) {

            })
        }

        function addHouse() {
            DashBoardService.addHouse(vm.new_house).then(function (response) {
                console.log(response);
            })
        }






    }]);
}();