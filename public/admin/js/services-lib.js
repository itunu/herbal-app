
'use strict';
!function() {
    var dashboardService = angular.module('HostelApp.services');
    dashboardService.service('DashBoardService',['$resource','$rootScope', 'StorageService',function ($resource,$rootScope,StorageService) {
        var token = StorageService.setDB('current_user').getRecord('data').token;
        var req = $resource('/api/:action/:id', {},{
            getHerbs: {method:'GET', headers:{'Authorization':token}, isArray:false, params:{action:'herbs'}},
            getHerb: {method:'GET', headers:{'Authorization':token}, isArray:false, params:{action:'herbs', id:'@id'}},
            addTMP: {method:'POST', headers:{'Authorization':token}, isArray:false, params:{action:'herbs'}},
        });


        return{
            getHerbs:getHerbs,
            getHerb:getHerb,
            addTMP:addTMP,

        };


        function getHerbs() {
            return  req.getHerbs().$promise;
        }
        function getHerb(params) {
            return  req.getHerbs(params).$promise;
        }


        function addTMP(params) {
            return req.addTMP(params).$promise;
        }


    }]);
}();

'use strict';
!function() {
    var loginService = angular.module('HostelApp.services');
    loginService.service('LoginService',['$resource','$rootScope',function ($resource,$rootScope) {
        var req = $resource('/:action', {},{
            login: {method:'POST',isArray:false, params:{action:'login'}}
        });


        return{
            login:login
        };


        function login(params) {
            return  req.login(params).$promise;
        }
    }]);
}();
'use strict';
!function() {
    var responseService = angular.module('HostelApp.services');
    responseService.service('ResponseService',['$rootScope',function ($rootScope) {

        return {
            Brew: Brew
        };

        function Brew(response) {
            return {
                isOk:isOk,
                getData:getData,
                getError:getError
            };

            function isOk() {
                return (response.status === 'success' &&
                    response.data &&
                    ( response.data.response || response.data.message)
                ) ? true : false;
            }

            function getData() {
                if(isOk()) {

                    return response.data.response || response.data.message;
                }
                return false;
            }

            function getError() {
                if(!isOk()) {
                    return response.data.message;
                }
                return "A network error has occured";
            }
        }
    }]);}()

'use strict';
!function() {
    var storageService = angular.module('HostelApp.services');
    storageService.service('StorageService', ['$rootScope', function ($rootScope) {
        var __local_storage = localStorage;


        return {
            createDB:createDB,
            setDB:setDB
        };

        function createDB(db_name) {
            if(__local_storage) {
                if(!__local_storage.getItem(db_name)) {
                    __local_storage.setItem(db_name, JSON.stringify({}));
                }
            }
        }

        function setDB(db_name) {
            createDB(db_name);
            return {
                saveRecord:saveRecord,
                getRecord:getRecord,
                hasRecord: hasRecord
            };

            function saveRecord(data_key, data) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        jRecord[data_key] = data;
                        __local_storage.setItem(db_name, JSON.stringify(jRecord));
                    }catch (e) {}
                }
            }

            function getRecord(key) {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        if(key in jRecord) { return jRecord[key];}
                    }catch (e) {}
                }
                return false;
            }

            function hasRecord() {
                var prevRecord = __local_storage.getItem(db_name);
                if(prevRecord) {
                    try {
                        var jRecord = JSON.parse(prevRecord);
                        return Object.keys(jRecord).length > 0;
                    }catch (e) {
                        return false;
                    }
                }
                return false;
            }
        }

    }]);
}();