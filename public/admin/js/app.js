'use strict';
!function () {
    var controllers = angular.module('HostelApp.controllers',[]);
    var services = angular.module('HostelApp.services',[]);
    var directives = angular.module('HostelApp.directives',[]);
    var app = angular.module('HostelApp',['ngRoute','ngResource','angularFileUpload','HostelApp.controllers','HostelApp.services','HostelApp.directives']);
    app.config(['$routeProvider','$interpolateProvider','$httpProvider','$locationProvider', function ($routeProvider,$interpolateProvider,$httpProvider,$locationProvider) {
        $routeProvider.when('/login',{
            templateUrl :'../admin/views/login.html'
        });
        $routeProvider.when('/register', {
           templateUrl:'../admin/views/register.html'
        });
        $routeProvider.when('/admin_dashboard', {
            templateUrl:'../admin/views/admin_dashboard.html'
        });
        $routeProvider.when('/manage_tmps', {
            templateUrl:'../admin/views/manage_tmp.html'
        });

        $routeProvider.when('/herb_details/:herb_id', {
            templateUrl:'../admin/views/manage_tmp.html'
        });

        $routeProvider.otherwise({
            redirectTo :'/login'
        });

        $locationProvider.html5Mode(true);
    }]);
}();