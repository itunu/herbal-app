/**
 * Created by itunu.babalola on 2/22/17.
 */
module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            angular: {
                src:['!js/angular-libs.js', 'bower_components/angular/angular.js', 'bower_components/angular-route/angular-route.js', 'bower_components/angular-resource/angular-resource.js', 'bower_components/angular-file-upload/dist/angular-file-upload.js'],
                dest:'js/angular-libs.js'
            },
            models:{
                src:['js/Models/*.js'],
                dest:'js/model.js'
            },
            controllers: {
                src:['js/Controllers/*.js'],
                dest:'js/controller-libs.js'

            },

            directives: {
                src:['js/Directives/*.js'],
                dest:'js/directives-libs.js'
            },

            services:{
                src: ['js/Services/*.js'],
                dest:'js/services-lib.js'
            }
        },

        uglify: {
            options : {
                sourceMap: false,
                mangle: false,
                compress: false,
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            angular:{
                src:'js/angular-libs.js',
                dest:'js/compiled/angular.min.js'
            },
            models:{
                src:['js/model.js'],
                dest:'js/compiled/model.min.js'
            },
            controllers:{
                src:'js/controller-libs.js',
                dest:'js/compiled/controller.min.js'

            },
            directives: {
                src:'js/directives-libs.js',
                dest:'js/compiled/directives.min.js'
            },
            app:{
                src:'js/app.js',
                dest:'js/compiled/app.min.js'
            },

            services:{
                src: 'js/services-lib.js',
                dest:'js/compiled/services.min.js'
            }
        }


    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.registerTask('default', [ 'concat', 'uglify']);
};